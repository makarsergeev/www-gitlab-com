features:
  secondary:
    - name: "Entropy Requirements for New User Passwords"
      available_in: [core, starter, premium, ultimate]
      documentation_link: 'https://docs.gitlab.com/ee/security/password_length_limits.html'
      gitlab_com: false
      reporter: mattgonzales
      stage: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/issues/36776'
      description: |
        Organizations need a way to secure their GitLab instances that aligns with their internal policies and procedures. Part of [securing GitLab](https://gitlab.com/groups/gitlab-org/-/epics/258) is enforcing a password policy. GitLab recently updated its own internal [password policy guidelines](https://about.gitlab.com/handbook/security/#gitlab-password-policy-guidelines) based on [NIST SP 800-63B](https://pages.nist.gov/800-63-3/sp800-63b.html). In this special publication, NIST advises on leveraging password length and complexity, but does not recommend password rotation or even requiring specific complexity rules (e.g. a specific number of special characters).

        Using NIST's guidance, GitLab is introducing a new setting within the Admin Area to specify a **minimum password length** that applies only to new passwords. This means any new account being created or any password being changed will be required to meet this minimum length requirement. By enabling customers to define a minimum password length, GitLab environments can become more secure and organizations can manage this policy across an instance for reassurance that passwords are compliant with internal policies.
