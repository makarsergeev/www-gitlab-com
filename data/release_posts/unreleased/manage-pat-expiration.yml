features:
  secondary:
    - name: "Require rotation of personal access tokens"
      available_in: [ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/user/admin_area/settings/account_and_limit_settings.html#limiting-lifetime-of-personal-access-tokens-ultimate-only'
      reporter: jeremy
      stage: manage
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/issues/3649'
      description: |
        Security-minded organizations have historically used regular rotation of credentials to limit the amount of time
        an attacker has access to a system through a compromised secret. While guidelines from organizations like [NIST](https://pages.nist.gov/800-63-3/sp800-63b.html) 
        no longer recommend periodic rotation, we're adding the ability to enforce regular 
        rotation of [personal access tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens) 
        due to their inherent lack of 2FA protection, customer demand, and importance in 
        certain compliance frameworks (e.g. [PCI](https://pciguru.wordpress.com/2019/03/11/the-new-nist-password-guidance/)).

        With this change, an instance administrator can configure a maximum lifetime for generated personal access tokens. Applying 
        a limit will expire existing tokens, which must be regenerated and adhere to the newly applied expiration requirement. After a 
        token's expiration date has passed, it must be regenerated.
